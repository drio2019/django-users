from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.index),
    path("logout", views.log_out),
    path("checklogin", views.logged_user),
    path("<str:key>", views.get_content),
    # Importante el orden, escribir de mas restrictivo a menos, para que no se solapen
]
