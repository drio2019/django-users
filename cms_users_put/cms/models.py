from django.db import models


class Content(models.Model):
    key = models.CharField(max_length=64)
    value = models.TextField()

    def __str__(self):
        return self.key


class Comment(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(blank=False)
    date = models.DateTimeField('published')
    content = models.ForeignKey(Content, on_delete=models.CASCADE)

    def __str__(self):
        return self.title + " -> " + self.content.__str__()
