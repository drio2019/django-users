from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Content, Comment
import datetime
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.shortcuts import redirect


@csrf_exempt
def get_content(request, key):
    keys = Content.objects.values_list('key', flat=True)
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        update_content(key, keys, value)
        keys = Content.objects.values_list('key', flat=True)
    elif request.method == "POST":
        if "value" in request.POST:
            value = request.POST.get('value')
            update_content(key, keys, value)
            keys = Content.objects.values_list('key', flat=True)
        if "title" and "body" in request.POST:
            title = request.POST.get('title')
            body = request.POST.get('body')
            comment = Comment(title=title, body=body, date=datetime.datetime.now(),
                              content=Content.objects.get(key=key))
            comment.save()
    if key in keys:
        content = Content.objects.get(key=key)
        template = loader.get_template('content.html')
        context = {
            'content': content,
            'comments': content.comment_set.all(),
            'authenticated': request.user.is_authenticated,
        }
        return HttpResponse(template.render(context, request))
    else:
        raise Http404("Resource not found for key: " + key)


def update_content(key, keys, value):
    if key in keys:
        content = Content.objects.get(key=key)
        content.value = value
    else:
        content = Content(key=key, value=value)
    content.save()


@csrf_exempt
def index(request):
    contents = Content.objects.all()
    template = loader.get_template('index.html')
    context = {
        'contents': contents
    }
    return HttpResponse(template.render(context, request))


def logged_user(request):
    if request.user.is_authenticated:
        return HttpResponse("Has iniciado sesion como " + request.user.username)
    else:
        return HttpResponse("No has iniciado sesion. <a href='/admin'>Inicia sesion aqui</a>")


def log_out(request):
    logout(request)
    return redirect('/cms/')
